package net.bencarson.cucumber;

import java.io.File;
import java.io.IOException;

import org.sikuli.script.Match;
import org.sikuli.script.Screen;

import cucumber.api.java.en.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import net.bencarson.cucumber.sikulix.CalibreLauncher;

public class Stepdefs {

	private CalibreLauncher myLauncher;
	
	@Before
	public void setUp() {
		myLauncher = new CalibreLauncher();
	}
	
	@Given("^Calibre is present$")
	public void Calibre_is_Present() {
		try {
			String calibreFilePath = 
					myLauncher.getCalibreDirFile().getAbsolutePath();
			System.out.println("Check for Calibre in file system");
			Assert.assertNotNull(calibreFilePath);
			Assert.assertTrue(calibreFilePath.length() > 0);
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}
	
	@When("^Calibre is launched$")
	public void Calibre_is_launched() {
		System.out.println("Launch Calibre");
		try {
			File calibreExecutable = myLauncher.getCalibreDirFile().getAbsoluteFile();
			myLauncher.launchCalibre(calibreExecutable);
			Assert.assertEquals(myLauncher.getCalibreApp().getName(), "calibre");
		} catch (IOException e) {
			Assert.fail(e.getMessage());
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
	@When("^Calibre is active$")
	public void calibre_is_active() {
		Assert.assertEquals(myLauncher.getCalibreApp().getName(), "calibre");
	}

	
	@Then("^Calibre is focused$")
	public void Calibre_is_focused() {
		System.out.println("Focus on Calibre app");
		myLauncher.getCalibreApp().focus();
		Screen testScreen = new Screen();
		testScreen.highlight(3);
		Match isCalibreFound = testScreen.exists(myLauncher.getCalibrePattern());
		Assert.assertNotNull(isCalibreFound);
		Assert.assertTrue(isCalibreFound.isValid());
		//close the application, so that subsequent calls to open the application will work
		int isClosedSuccessfully = myLauncher.closeCalibre();
		Assert.assertEquals(1, isClosedSuccessfully);
	}
	@Then("^Calibre is closed$")
	public void calibre_is_closed() {
	    int isClosedSuccessfully = myLauncher.closeCalibre();
	    Assert.assertEquals(1, isClosedSuccessfully);
	}
}
