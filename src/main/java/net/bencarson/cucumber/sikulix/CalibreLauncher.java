package net.bencarson.cucumber.sikulix;

import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import org.sikuli.script.App;
import org.sikuli.script.Pattern;

public class CalibreLauncher {

	private String detectedOS;
	//this is the location of the images that Sikuli will use for comparisons
	private StringBuilder imagePath;
	private Logger _log = Logger.getLogger(CalibreLauncher.class.getName());
	private App calibreApp;
	//using StringBuilder because I don't care about thread safety; the text isn't going to change
	private StringBuilder winCalibreFilePath;
	private StringBuilder osxCalibreFilePath;
	private File calibreDirFile;
	//this pattern represents the header bar of the Calibre app
	private Pattern calibrePattern;
	
	/****************
	 * Constructors *
	 ****************/
	
	public CalibreLauncher() {
		//init method for this bot
		initCalibreLauncher();
	}
	
	public CalibreLauncher(File calibreDir, StringBuilder clbrFilePath) {
		setCalibreDirectory(calibreDir);
		setCalibreFilePath(clbrFilePath);
	}

	/**
	 * I'm realizing in hindsight the whole use of separatorChar is pointless,
	 * as I have totally separate variables for each operating system...
	 * @return void
	 */
	private void initCalibreLauncher() {
		detectedOS = System.getProperty("os.name");
		_log.info("Detected OS: " + detectedOS);
		winCalibreFilePath = new StringBuilder("G:")
				.append(File.separatorChar)
				.append("dev")
				.append(File.separatorChar)
				.append("tools")
				.append(File.separatorChar)
				.append("Calibre Portable");
		osxCalibreFilePath = new StringBuilder(File.separatorChar)
				.append(File.separatorChar)
				.append("Applications");
				
		if(detectedOS.equalsIgnoreCase("Mac OS X")) {
			_log.info("Searching for Calibre in " + osxCalibreFilePath.toString());
			calibreDirFile = new File(osxCalibreFilePath.toString());
		} else {
			_log.info("Searching for Calibre in " + winCalibreFilePath.toString());
			calibreDirFile = new File(winCalibreFilePath.toString());
		} //TODO: write logic for Linux/Unix
		
		imagePath = new StringBuilder("src")
				.append(File.separatorChar)
				.append("main")
				.append(File.separatorChar)
				.append("resources")
				.append(File.separatorChar)
				.append("images");
		
		calibrePattern = new Pattern(imagePath.toString() + File.separatorChar + "OpenCalibre.PNG");
	}
	
	/**
	 * 
	 * @param calibreDir
	 * @return
	 * @throws IOException
	 */
	public boolean launchCalibre(File calibreDir) throws IOException {
		boolean isAppFound = false;
		//first, check if App is available (has process ID), before launching
		if(getCalibreApp().getPID()<0) {
			_log.info("calibre folder is " + calibreDirFile.getAbsolutePath());
			ProcessBuilder calibreBuilder = null;
			if(detectedOS.equalsIgnoreCase("Mac OS X")) {
				calibreBuilder =
						new ProcessBuilder("open", calibreDirFile.getAbsolutePath()+File.separatorChar+"calibre.app"); 
			} else if(detectedOS.equalsIgnoreCase("Windows 8.1")) {
				calibreBuilder = 
						new ProcessBuilder(calibreDirFile.getAbsolutePath()+File.separatorChar+"calibre-portable.exe");
			}
			calibreBuilder.directory(calibreDirFile);
			Process calibre = calibreBuilder.start();
		}
		if(getCalibreApp().focus() != null) {
			isAppFound = true;
		}
		return isAppFound;
	}
	
	/**
	 * 
	 * @return
	 */
	public int closeCalibre() {
		return App.close("calibre");
	}
	
	/********************
	 * Accessor methods *
	 ********************/
	
	/**
	 * Assumes that the calibreDirectory File is set
	 * @return Java File instance of the Calibre executable
	 * @throws Exception 
	 */
	public File getCalibreDirFile() throws Exception {
		return calibreDirFile;
	}
	
	/**
	 * 
	 * @param calibreDirectory
	 */
	public void setCalibreDirectory(File calibreDirectory) {
		this.calibreDirFile = calibreDirectory;
	}

	
	
	/**
	 * set the value of the File that represents the directory in which calibre exists
	 * @param calDir
	 */
	public void setCalibreDirFile(File calDir) {
		calibreDirFile = calDir;
	}
	
	/**
	 * set the value of the string that represents the Calibre directory
	 * @param calPath is a string that represents a file path to the Calibre executable
	 */
	public void setCalibreFilePath(StringBuilder calPath) {
		winCalibreFilePath = calPath;
	}
	
	/**
	 * 
	 * @param clbrApp is a Sikuli App representing a running instance of Calibre  
	 */
	public void setCalibreApp(App clbrApp) {
		calibreApp = clbrApp;
	}
	
	/**
	 * Gets an instance of the App 'calibre' that is currently referenced by this launcher
	 * @return
	 */
	public App getCalibreApp() {
		if(calibreApp == null) {
			calibreApp = new App("calibre");
		}
		return calibreApp;
	}
	
	/**
	 * return the Pattern object that represents the header bar of the calibre app.
	 * This is what Sikuli will look for when checking for the app
	 * @return
	 */
	public Pattern getCalibrePattern() {
		if(calibrePattern == null) {
			//default calibrepattern
			calibrePattern = new Pattern(imagePath.toString() + File.separatorChar + "OpenCalibre.PNG");
		}
		return calibrePattern;
	}
	
	/**
	 * define the Pattern that represents the header bar of the calibre app
	 * this is what Sikuli will look for when checking for the app
	 */
	public void setCalibrePattern(Pattern calImage) {
		calibrePattern = calImage; 
	}
}
