## Cucumber.io Sandbox Project
***Behavior Driven Development*** (BDD) sounds like an awesome solution to several of the issues I'm facing in my day job.<br>
[Cucumber](https://cucumber.io) seems to be a pretty popular, well-documented, and broadly-adopted framework for doing BDD.<br>
<BR>
Here's where I'll try it out and see if I has potential for my needs or not.
